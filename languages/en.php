<?php   
// Invoice page
$Lang['page_title'] = 'Yuki - Invoice ship';
$Lang['page_keywords'] ='Invoice, Yuki, send';
$Lang['page_description'] = 'Send invoice to Yuki and customer';
$Lang['FORM_TITLE'] = 'Send invoice';

// email
$Lang['mail_title'] = 'Title';
$Lang['mail_subjet'] = 'Email subject';

// Formfields
$Lang['select'] = '--select--';
$Lang['yes'] = 'Yes';
$Lang['no'] = "No";
$Lang['SendTo'] = 'Send to';
$Lang['SendToBookkeeper'] = 'Only Yuki';
$Lang['SendToBookkeeperCustomer'] = 'To Yuki and the customer';
$Lang['company'] = 'Company';
$Lang['contact name'] = 'Contact';
$Lang['contact email'] = 'Email address';
$Lang['file attachment'] = 'Attachment';
$Lang['invoicenr'] = 'Invoice number';
$Lang['paymentnr'] = 'Payment by';
$Lang['paypal memo'] = 'PayPal = Comma Separated - payments that affect the balance';
$Lang['bookingnr'] = 'Books like';
$Lang['dMessage'] = 'Post content to the customer';
$Lang['yMessage'] = 'Note for Yuki auditor';
$Lang['attention'] = 'Extra attention';
$Lang['submit'] = 'Submit';
$Lang['reset'] = 'Emptying';

// check
$Lang['required'] = 'required';
$Lang['required_field'] = 'This field is required';
$Lang['required_letters'] ='Only letters and spaces';
$Lang['required_email'] = 'This should be an email address';

// errors
$Lang['error_attachment'] = 'No Annex present';

// messages
$Lang['msg_smtp_correct'] = 'The email was sent';
$Lang['msg_smtp_fail'] = 'Something went wrong. Contact your webmaster. PHPMail () failed .: ';
$Lang['errorSendToBookkeeperCustomer'] = 'An error in the mail to the customer and Yuki';
$Lang['mailSendToBookkeeperCustomer'] = 'The mail is sent to the customer and Yuki';
$Lang['errorSendToBookkeeper'] = 'An error in the mail to Yuki';
$Lang['mailSendToBookkeeper'] = 'The mail was sent to Yuki';

?>