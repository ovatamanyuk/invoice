<?php
// Include libs
require  '../phpmailer/class.phpmailer.php';
include ('language.php');
include ("../".$languageFile);

// Get data from form
$formSendto = isset($_POST['sendto']) ? $_POST['sendto'] : false;
$company = isset($_POST['company']) ? $_POST['company'] : false;
$contactname = isset($_POST['contactname']) ? $_POST['contactname'] : false;
$contactmail = isset($_POST['contactmail']) ? $_POST['contactmail'] : false;
$invoicenr = isset($_POST['invoicenr']) ? $_POST['invoicenr'] : false;
$paymentnr = isset($_POST['paymentnr']) ? $_POST['paymentnr'] : false;
$dMessage = isset($_POST['dMessage']) ? $_POST['dMessage'] : false;
$attention = isset($_POST['attention']) ? $_POST['attention'] : false;
$yMessage = isset($_POST['yMessage']) ? $_POST['yMessage'] : false;
$bookingnr = isset($_POST['bookingnr']) ? $_POST['bookingnr'] : false;


// Is file loaded
if(is_uploaded_file($_FILES["attachmentFile"]["tmp_name"]))
{
    move_uploaded_file($_FILES["attachmentFile"]["tmp_name"], "../temp/".$_FILES["attachmentFile"]["name"]);
    $attachmentFile = $_FILES["attachmentFile"]["name"];
} else {
    echo("Error to load file");
}
// Check data


if ($formSendto == 1 || !$formSendto) { //customer

    /*

		check is everything is filled in

		send eMail to customer

		subject = $invoicenr
		message = all info

		show message that mail is send to customer

	*/
    $file = '../data/data3.csv';
    if (file_exists($file)){
        $row = 1;
        $data1 = '';
        if (($handle = fopen("../data/data3.csv", "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                $num = count($data);
                $row++;
                for ($c = 0; $c < $num; $c++) {
                    $data1 .= $data[$c].",";
                }
            }
        }

        $arr = explode(",", $data1);
        if (in_array($company, $arr)) {

        }else{
            $current = file_get_contents($file);
            $current .= $company.",".$contactname.",".$contactmail."\n";
            file_put_contents($file, $current);
        }
    }
    else{
        $current .= $company.",".$contactname.",".$contactmail."\n";
        file_put_contents($file, $current);
    }

    $message = '<html><body>';
    if($attention = 1){
        $message .= '<h1>##LET OP ##</h1>';
    }
    $message .= '<table rules="all" style="border-color: #666;" cellpadding="10">';
    $message .= "<tr style='background: #eee;'><td><strong>". $lang['company'] ."</strong> </td><td>" . $company ? $company : false . "</td></tr>";
    $message .= "<tr><td><strong>". $lang['contactname'] ."</strong> </td><td>" . $contactname ? $contactname : false . "</td></tr>";
    $message .= "<tr><td><strong>". $lang['paymentnr'] ."</strong> </td><td>" . $paymentnr ? $paymentnr : false . "</td></tr>";
    $message .= "<tr><td><strong>". $lang['dMessage'] ."</strong> </td><td>" . $dMessage ? $dMessage : false . "</td></tr>";
    $message .= "<tr><td><strong>". $lang['yMessage'] ."</strong> </td><td>" . $yMessage ? $yMessage : false . "</td></tr>";
    $message .= "<tr><td><strong>". $lang['bookingnr'] ."</strong> </td><td>" . $bookingnr ? $bookingnr : false . "</td></tr>";
    $message .= "</table>";
    $message .= "</body></html>";
    $message = '<html><body>';

    // Send mail
    if(!$contactmail){
        exit('Fatal error');
    }

    $mail = new PHPMailer();
    require ('config.php');
    $mail->From      = 'you@example.com';
    $mail->FromName  = $invoicenr;
    $mail->Subject   = $subject;
    $mail->Body      = $message;
    $mail->AddAddress( $contactmail, 'bookkeeper@gmail.com' );

//    $file_to_attach = "../temp/".$attachmentFile;
//
//    $mail->AddAttachment( $file_to_attach , $attachmentFile );

    $mail->Send();

}

if ($formSendto == 2 ) { //bookkeeper
	/*

		hide field $company, $contactname, $contactmail
		check is everything is filled in

		send eMail only to $bookkeeperEmail

		subject = $invoicenr
		message = all info

		if $attention is 'Yes' then ->  message = attention  + message

		show message that mail is to bookkeeper

	*/

    $message = '<html><body>';
    if($attention = 1){
        $message .= '<h1>##LET OP ##</h1>';
    }
    $message .= '<table rules="all" style="border-color: #666;" cellpadding="10">';
    $message .= "<tr><td><strong>". $lang['paymentnr'] ."</strong> </td><td>" . $paymentnr ? $paymentnr : false . "</td></tr>";
    $message .= "<tr><td><strong>". $lang['dMessage'] ."</strong> </td><td>" . $dMessage ? $dMessage : false . "</td></tr>";
    $message .= "<tr><td><strong>". $lang['yMessage'] ."</strong> </td><td>" . $yMessage ? $yMessage : false . "</td></tr>";
    $message .= "<tr><td><strong>". $lang['bookingnr'] ."</strong> </td><td>" . $bookingnr ? $bookingnr : false . "</td></tr>";
    $message .= "</table>";
    $message .= "</body></html>";
    $message = '<html><body>';

    // Send mail

    $mail = new PHPMailer();
    require ('config.php');
    $mail->From      = 'you@example.com';
    $mail->FromName  = $invoicenr;
    $mail->Subject   = $subject;
    $mail->Body      = $message;
    $mail->AddAddress( 'bookkeeper@gmail.com' );

    $file_to_attach = "../temp/".$attachmentFile;

    $mail->AddAttachment( $file_to_attach , $attachmentFile );

    $mail->Send();

}



/*
	
	now log data where and what filename is send
	!! the file does not have to be saved only the name.
	save to a log file will be okey

	when message is logged and send then return to form for new message
	
		
	
*/



?>