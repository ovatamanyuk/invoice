<?php
	
$available_langs = array('en','nl'); // What languages do we support
$dataFolder = 'data'; // Where can you find the field list data

//##### start - Get Language
	
	if( isset( $_GET[ 'lang' ] ) ) {
	    $_SESSION[ 'WPLANG' ] = $_GET[ 'lang' ]; 
	    $locale = $_SESSION[ 'WPLANG' ];
	    $languagemsg = 'based on url parameter';
	} else {
		// Set a default value
		$_SESSION[ 'WPLANG' ] = 'nl';

	    if( isset( $_SESSION[ 'WPLANG' ] ) ) {
	        $locale = $_SESSION[ 'WPLANG' ];
	        $languagemsg = 'based on session variable';
	    } else { 
	        $browserlang = substr($_SERVER["HTTP_ACCEPT_LANGUAGE"],0,2);
	        $_SESSION[ 'WPLANG' ] = $browserlang;
	        $locale = $browserlang;
	        $languagemsg = 'based on browser language';
	    }
	};


	
	if($locale != ''){
	    if(in_array($locale, $available_langs)) {
	        $locale = $locale;
	    }
	    else {
	        $languagemsg = 'no language available';
			echo "<h1>$languagemsg</h1>";
		    exit;
	    }
	}
	
	$languageFile = 'languages/' . $locale .'.php';
	
	if($demo){
		echo $languagemsg . '<br>';
		echo $locale . '<br>';
		echo $languageFile . '<br>';
		
	}
	
	
?>	