<?php
session_start();
// config
include('helpers/language.php');
include($languageFile);
// config
require('helpers/config.php');

$colA = '2';
$colB = '10';
$colC = '12';
$colD = '12';
$colE = '12';

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN">

<html>
<head>
    <title><?php echo $lang['page_title']; ?></title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="keywords" content="<?php echo $lang['page_keywords']; ?>">
    <meta name="description" content="<?php echo $lang['page_description']; ?>">
    <meta name="author" content="Schneider Webdesign">

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link media="all" type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.6/themes/smoothness/jquery-ui.css" rel="stylesheet"/>
</head>

<body>
<div class="wrapper col-lg-12">
    <div id="contact-form" class="col-lg-12">

        <h3><?php echo $lang['form_title']; ?></h3>

        <div id="mail-status"></div>

        <form id="frmContact" action="/helpers/sendmail.php" method="post" enctype="multipart/form-data">
            <div class="row">
                <div class="col-lg-8 col-md-8">
                    <div class="form-group">
                        <div class="col-lg-<?php echo $colC; ?>" data-toggle="buttons">
                            <label class="btn btn-primary customer">
                                <input name="sendto" value="1"
                                       type="radio"><?php echo $lang['SendToBookkeeperCustomer']; ?>
                            </label>
                            <label class="btn btn-primary bookkeeper">
                                <input name="sendto" value="2" type="radio"><?php echo $lang['SendToBookkeeper']; ?>
                            </label>
                        </div>
                    </div>
                    <br>
                    <br>
                    <div class="form-group to-hide">
                        <label class="col-lg-<?php echo $colA; ?> control-label"><?php echo $lang['company']; ?>
                            :</label>
                        <div class="col-lg-<?php echo $colB; ?>">
                            <input id="company" name="company" placeholder="<?php echo $lang['company']; ?>"
                                   value='<?php echo $company ?>' type="text"
                                   data-validation="required" onblur="prefill()">
                            <!--

                               make this field a selection scrolldown from data3.csv
                               when select company fill in $contactname and @contactmail

                               but also the posibility to add free text.


                            -->
                        </div>
                    </div>
                    <div class="form-group to-hide">
                        <label class="col-lg-<?php echo $colA; ?> control-label"><?php echo $lang['contactname']; ?>
                            :</label>
                        <div class="col-lg-<?php echo $colB; ?>">
                            <input id="contactname" name="contactname" placeholder="<?php echo $lang['contactname']; ?>"
                                   value='<?php echo $contactname ?>' type="text" data-validation="required">
                        </div>
                    </div>
                    <div class="form-group to-hide">
                        <label class="col-lg-<?php echo $colA; ?> control-label"><?php echo $lang['contactmail']; ?>
                            :</label>
                        <div class="col-lg-<?php echo $colB; ?>">
                            <input id="contactmail" name="contactmail" placeholder="<?php echo $lang['contactmail']; ?>"
                                   value='<?php echo $contactmail ?>' type="email" data-validation="email">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-<?php echo $colA; ?> control-label"><?php echo $lang['attachmentFile']; ?>
                            :</label>
                        <div class="col-lg-<?php echo $colB; ?>">
                            <input name="attachmentFile" id="attachmentFile"
                                   placeholder="<?php echo $lang['attachmentFile']; ?>"
                                   value='<?php echo attachmentFile ?>' type="file" data-validation="required">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-<?php echo $colA; ?> control-label"><?php echo $lang['invoicenr']; ?>
                            :</label>
                        <div class="col-lg-<?php echo $colB; ?>">
                            <input name="invoicenr" id="invoicenr" placeholder="<?php echo $lang['invoicenr']; ?>"
                                   type="text" data-validation="required">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-<?php echo $colA; ?> control-label"><?php echo $lang['paymentnr']; ?>
                            :</label>
                        <div class="col-lg-<?php echo $colB; ?>">
                            <?php
                            $row = 1;
                            if (($handle = fopen($dataFolder . "/data1.csv", "r")) !== FALSE) {
                                while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                                    $num = count($data);
                                    $row++;
                                    for ($c = 0; $c < $num; $c++) {
                                        if ($paymentnr == $data[$c]) {
                                            $checked = 'checked';
                                        } else {
                                            $checked = '';
                                        }
                                        ?>
                                        <label class="radio-btn exWidth"><input name="paymentnr" <?php echo $checked ?>
                                                                                value="<?php echo $data[$c] ?>"
                                                                                type="radio"
                                                                                data-validation="required"> <?php echo $data[$c] ?>
                                        </label>
                                        <?php
                                    }
                                }
                                fclose($handle);
                            }
                            ?>
                            <span class="paypalMemo"><?php echo $lang['paypalMemo']; ?></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-<?php echo $colA; ?> control-label"><?php echo $lang['dMessage']; ?>
                            :</label>
                        <div class="col-lg-<?php echo $colB; ?>">
                            <?php
                            $row = 1;
                            if (($handle = fopen($dataFolder . "/data4.csv", "r")) !== FALSE) {
                                while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                                    $num = count($data);
                                    $row++;
                                    for ($c = 0; $c < $num; $c++) {
                                        if ($paymentnr == $data[$c]) {
                                            $checked = 'checked';
                                        } else {
                                            $checked = '';
                                        }
                                        ?>
                                        <label class="radio-btn exWidth"><input name="dMessage" <?php echo $checked ?>
                                                                                value="<?php echo $data[$c] ?>"
                                                                                type="radio"
                                                                                data-validation="required"> <?php echo $data[$c] ?>
                                        </label>
                                        <?php
                                    }
                                }
                                fclose($handle);
                            }
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-<?php echo $colA; ?> control-label"><?php echo $lang['attention']; ?>
                            :</label>
                        <div class="col-lg-<?php echo $colB; ?>" data-toggle="buttons">
                            <label class="btn btn-primary">
                                <input name="attention" value="1" type="radio"
                                       data-validation="required"><?php echo $lang['yes']; ?>
                            </label>
                            <label class="btn btn-primary">
                                <input name="attention" value="2" type="radio"
                                       data-validation="required"><?php echo $lang['no']; ?>
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-<?php echo $colA; ?> control-label"><?php echo $lang['yMessage']; ?>
                            :</label>
                        <div class="col-lg-<?php echo $colB; ?>">
                            <input name="yMessage" placeholder="<?php echo $lang['yMessage']; ?>"
                                   value='<?php echo yMessage ?>' type="text" data-validation="required">
                        </div>
                    </div>

                </div>
                <div class="col-lg-4 col-md-4">
                    <div class="form-group rightCol">
                        <label class="col-lg-<?php echo $colD; ?> control-label"><?php echo $lang['bookingnr']; ?>
                            :</label>
                        <div class="col-lg-<?php echo $colE; ?>">
                            <?php
                            $row = 1;
                            if (($handle = fopen($dataFolder . "/data2.csv", "r")) !== FALSE) {
                                while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                                    $num = count($data);
                                    $row++;
                                    for ($c = 0; $c < $num; $c++) {
                                        if ($bookingnr == $data[$c]) {
                                            $checked = 'checked';
                                        } else {
                                            $checked = '';
                                        }
                                        ?>
                                        <label class="radio-btn exWidth"><input name="bookingnr" <?php echo $checked ?>
                                                                                value="<?php echo $data[$c] ?>"
                                                                                type="radio"
                                                                                data-validation="required"> <?php echo $data[$c] ?>
                                        </label>
                                        <?php
                                    }
                                }
                                fclose($handle);
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
            <p>
                <input name="submit" class="col-lg-2 btn btn-success" type="submit" id="contact-submit"
                       value="<?php echo $lang['submit']; ?>">
                <input name="reset" class="col-lg-2 btn btn-danger" type="reset" id="contact-reset"
                       value="<?php echo $lang['reset']; ?>">
            <div class="col-lg-2" id="loader-icon" style="display:none;"><img src="loader.gif"/></div>
            </p>
        </form>
    </div>
</div>


<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>

<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.23/jquery.form-validator.min.js"></script>


<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

<?php
$row = 1;
$data1 = '';
$autocompletedata = '';
if (($handle = fopen($dataFolder . "/data3.csv", "r")) !== FALSE) {
    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
        $num = count($data);
        $row++;
        for ($c = 0; $c < $num; $c++) {
            $data1 .= $data[$c].",";
        }
        for ($c = 0; $c < $num; $c++) {
            $autocompletedata .= "\"".$data[$c]."\"".",";
            $c = $c + 2;
        }
    }
    fclose($handle);
}
?>

<script>
    $.validate({
        modules: 'location, date, security, file',
        lang: 'nl',
    });
    $(".bookkeeper").click(function () {
        $(".to-hide").addClass("hide");
    });
    $(".customer").click(function () {
        $(".to-hide").removeClass("hide");
    });
</script>

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.3/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.6/jquery-ui.min.js"></script>

<!-- necessary for form -->
<script type="text/javascript">
    $(function() {
        $('#company').autocomplete({
            source: [<?php echo $autocompletedata ?>
            ],
            minLength: 0
        }).focus(function(){
            $(this).data("autocomplete").search($(this).val());
        });
    });
</script>

<script type="text/javascript">
    var filedata = "<?php echo $data1;?>";
    var arr = filedata.split(',');

    var company;
    var companyid;

    function prefill() {
        company = document.getElementById("company").value;
        companyid = arr.indexOf(company);
        var isSelected = arr.length - companyid;
        if(companyid >= 0 & isSelected != 1){
            document.getElementById("contactname").value  = arr[companyid+1];
            document.getElementById("contactmail").value  = arr[companyid+2];
        }else if (companyid < arr.length){

        }else{
            document.getElementById("contactname").value  = "";
            document.getElementById("contactmail").value  = "";
        }
    }
</script>

</body>
</html>
